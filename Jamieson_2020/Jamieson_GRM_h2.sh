# This script was used to create and visualize genome relatedness matrices and estimate heritability from them in REML modelling

# Start with a vcf file and tab-delimeted phenotype and covariate files
# If, as in this case, the vcf file isn't at the chromosomal level (i.e scaffold or contig) 
# you will have to use awk to prefix 'contig' on the data in the "chromosome" column of the vcf file for PLINK to work (--allow-extra-chr)

# Use plink to create the binary fileset (bim, bed, fam; bfile) from the vcf file 
# Use sed to remove the 'contig' prefix from the bim file
# Use mv to rename the bim file back to match the fileset
# Use GCTA to make the GRM
# Use GCTA to run the REML



# Adds the word contig to the begining of all lines in first column 
# NOTE manual removement of contig from header lines is required
# Used as a work around to the chr set issues
awk '{$1="contig" $1;}1' 'OFS=\t' < male_female_anticosti_LM_1.recode.vcf > male_female_anticosti_LM_1.vcf;


# Generate PLINK binary file set: bed, bim, and fam
./plink --vcf male_female_anticosti_LM_1.vcf --make-bed --allow-extra-chr --out male_female_anticosti_LM_1;

# Remove the contig string as is only needed for plink but is not allowed for GCTA
sed 's/contig//' < male_female_anticosti_LM_1.bim > male_female_anticosti_LM_1.temp.bim;

# Rename to remove the suffix "_temp" to recomplete the bfile set
mv male_female_anticosti_LM_1.temp.bim  male_female_anticosti_LM_1.bim;

# Generate the GRM 
./gcta64 --bfile male_only --autosome-num 227 --make-grm --out male_only;


# Filtering:

# Loci missingness filter is applied at the vcf file stage
vcftools --vcf male_female_anticosti.vcf  --max-missing 1 --out male_female_anticosti --recode;
./plink --vcf male_female_anticosti.vcf --make-bed --allow-extra-chr --out male_female_anticosti;
sed 's/contig//' < male_female_anticosti.bim > male_female_anticosti.temp.bim;
mv male_female_anticosti.temp.bim  male_female_anticosti.bim;


# Minor allele frequency filter is applied at the GRM creation stage
./gcta64 --bfile male_female_anticosti --autosome-num 227 --maf 0.05 --make-grm --out male_female_anticosti; 


# REML runs for male and female grm
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 1 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_dressed_body_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 2 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_total_length_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 3 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_tail_length_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 4 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_tail_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 5 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_hind_foot_length_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 6 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_thorax_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 7 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_rump_fat_10_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 8 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_rump_fat_5_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 9 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_rump_fat_5_10_reml_age_sex 
./gcta64 --reml --grm male_female_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 10 --reml-est-fix --qcovar covariate_age.qcovar --covar covariate_sex.covar --out male_female_peroneus_muscle_mass_reml_age_sex 

# REML runs for male only grm
./gcta64 --reml --grm male_only_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 11 --reml-est-fix --qcovar covar_srp.qcovar --out male_only_antlerspread_reml_srp_covar
./gcta64 --reml --grm male_only_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 12 --reml-est-fix --qcovar covar_srp.qcovar --out male_only_numberpoints_reml_srp_covar
./gcta64 --reml --grm male_only_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 13 --reml-est-fix --qcovar covar_srp.qcovar --out male_only_mainbeam_reml_srp_covar
./gcta64 --reml --grm male_only_anticosti --pheno male_female_multiple_phenotypes.phen --mpheno 14 --reml-est-fix --qcovar covar_srp.qcovar --out male_only_distalmainbeam_reml_srp_covar



# R


# R function from GCTA Manual
# Used to visualize the GRMs
ReadGRMBin=function(prefix, AllN=F, size=4){
 sum_i=function(i){
 return((1+i)*i/2)
 }

  BinFileName=paste(prefix,".grm.bin",sep="")
  NFileName=paste(prefix,".grm.N.bin",sep="")
  IDFileName=paste(prefix,".grm.id",sep="")
  id = read.table(IDFileName)
  n=dim(id)[1]
  BinFile=file(BinFileName, "rb");
  grm=readBin(BinFile, n=n*(n+1)/2, what=numeric(0), size=size)
  NFile=file(NFileName, "rb");
  if(AllN==T){
    N=readBin(NFile, n=n*(n+1)/2, what=numeric(0), size=size)
  }
  else N=readBin(NFile, n=1, what=numeric(0), size=size)
  i=sapply(1:n, sum_i)
  return(list(diag=grm[i], off=grm[-i], id=id, N=N))
}




# Create objects for each GRM with the above function
male<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/finalGRMs/male_only_anticosti");
lm70<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/finalGRMs/male_female_anticosti_lm70");
lm80<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/finalGRMs/male_female_anticosti_lm80");
lm90<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/finalGRMs/male_female_anticosti_lm90");
lm100<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/finalGRMs/male_female_anticosti_lm100");
lm70maf2<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/finalGRMs/male_female_anticosti_lm70maf2");
lm70maf5<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/finalGRMs/male_female_anticosti_lm70maf5");
lm70maf10<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/finalGRMs/male_female_anticosti_lm70maf10");
lm80maf5<-ReadGRMBin("/home/aidan/Downloads/currentThesis/WTD_GCTA/compare_vcf_files/fam_test/maf5/male_female_anticosti");

# Plot histograms of the GRM's 
tiff("/home/aidan/Downloads/currentThesis/FIGURES&TABLES/final_8_panel_relations.tiff", width= 6440, height=3080, res=300);
par(mfrow=c(2,4));
hist(lm70$off, breaks=100,main="LM 70% MAF 1%", xlab="Relatedness", ylab="Count", col="deeppink", xlim=c(-0.1, 0.5), cex.lab=1.5, cex.axis=1.2, cex.main=2);
hist(lm70maf5$off, breaks=100,main="LM 70% MAF 5%", xlab="Relatedness",ylab="Count",  col="deeppink", xlim=c(-0.1, 0.5), cex.lab=1.5, cex.axis=1.2, cex.main=2);
hist(lm70maf10$off, breaks=100,main="LM 70% MAF 10%", xlab="Relatedness", ylab="Count",col="deeppink", xlim=c(-0.1, 0.5), cex.lab=1.5, cex.axis=1.2, cex.main=2);
hist(lm80$off, breaks=100,main="LM 80% MAF 1%", xlab="Relatedness", ylab="Count", col="deeppink", xlim=c(-0.1, 0.5), cex.lab=1.5, cex.axis=1.2, cex.main=2);
hist(lm90$off, breaks=100,main="LM 90% MAF 1%", xlab="Relatedness", ylab="Count",col="deeppink", xlim=c(-0.1, 0.5), cex.lab=1.5, cex.axis=1.2, cex.main=2);
hist(lm100$off, breaks=100,main="LM 100 MAF 1%", xlab="Relatedness", ylab="Count", col="deeppink", xlim=c(-0.1, 0.5), cex.lab=1.5, cex.axis=1.2, cex.main=2);
hist(lm80maf5$off, breaks=100,main="LM 80% MAF 5%", xlab="Relatedness", ylab="Count", col="deeppink", xlim=c(-0.1, 0.5), cex.lab=1.5, cex.axis=1.2, cex.main=2);
hist(male$off, breaks=100,main="Male Only", xlab="Relatedness", ylab="Count", col="Purple", xlim=c(-0.1, 0.5), cex.lab=1.5, cex.axis=1.2, cex.main=2);
dev.off();
